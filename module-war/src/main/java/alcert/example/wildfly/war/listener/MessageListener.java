package alcert.example.wildfly.war.listener;

import org.jboss.annotation.ejb.ResourceAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;

@MessageDriven(
        messageListenerInterface = javax.jms.MessageListener.class,
        activationConfig = {
                @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
                @ActivationConfigProperty(propertyName = "useJNDI", propertyValue = "true"),
                @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jboss/test"),
                @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")})
@ResourceAdapter(value = "wmq.jmsra.rar")
public class MessageListener implements javax.jms.MessageListener {

    private static final Logger LOG = LoggerFactory.getLogger(MessageListener.class);

    @Override
    public void onMessage(Message message) {
        try {
            LOG.info("Get message: {}", message.getJMSMessageID());
        } catch (JMSException e) {
            LOG.info("Fail get JmsMessageId");
        }
    }
}
