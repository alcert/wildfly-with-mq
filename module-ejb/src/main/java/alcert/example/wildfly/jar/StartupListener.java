package alcert.example.wildfly.jar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Startup
@Singleton
public class StartupListener {

    private static final Logger LOG = LoggerFactory.getLogger(StartupListener.class);

    public StartupListener() {
        LOG.info("INFO ************** Construct new StartupListener... {}", hashCode());
    }

    @Schedule(hour = "*", minute = "*/5", persistent = false)
    public void scheduleRunning() {
        LOG.info("Shedule Run...");
    }

    @PostConstruct
    public void init() {
        LOG.info("PostConstract StartupListener start: {}", getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
    }

    @PreDestroy
    public void destroy() {
        LOG.info("PreDestroy StartupListener...");
    }
}
