package alcert.example.wildfly.jar.helpers;

import java.util.UUID;

/**
 *
 */
public final class AppHelper {
    private AppHelper() {
    }
    public static String randomUUID() {
        return UUID.randomUUID().toString();
    }
}
