# Запуск
docker-compose up -d --build
# Остановка
docker-compose down --rmi local
# Вход в терминал сервера, на котором jboss
docker exec -it temp_jboss_web-server_1 bash
# После сборки проекта, т.к. директория модуля ear смонтирована с /tmp/ear,
# для установки приложения достаточно запустить команду из терминала сервера:
/opt/jboss/wildfly/bin/jboss-cli.sh --connect --command="deploy --force /tmp/ear/target/module-ear-v1.ear"

# Вывод на экран содержимого терминала сервера, на котором jboss
docker logs temp_jboss_web-server_1
